package hometask6;

import java.util.Arrays;

class Family {

    private  Human mother;
    private  Human father;
    private  Human[] children;
    private  Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
        children = new Human[0];

    }

    public Human getMother(){

        return  mother;
    }

    public  Human getFather(){
        return  father;
    }

    public Human [] getChildren(){
        return  children;
    }

    public  Pet getPet (){

        return pet;
    }

    public void setPet(Pet familyPet){

        pet = familyPet;
    }


    public  void addChild(Human child) {

        Human[] newChildren = new Human[children.length + 1];
        for (int i = 0; i < children.length; i++) {
            newChildren[i] = children[i];
        }
        newChildren[newChildren.length -1] = child;
        children = newChildren;

        child.setFamily(this);
    }

    public int countFamily(Human [] children){

        return children.length + 2;
    }

    public boolean deleteChild( int index){
        if(index < 0 || children == null || index > (children.length -1)){
            return false;
        }
        children[index].setFamily(null);

        int length = children.length;
        Human [] newChildren = new Human[children.length - 1];

        for (int i = 0; i < index; i++) {
            newChildren[i] = children[i];
        }
        for (int i = index; i < children.length - 1; i++) {
            newChildren[i] = children[i + 1];
        }

        children = newChildren;
        if(length > children.length){
            return true;
        } else return false;
    }
    public boolean deleteChild( Human child){

        if(!Arrays.asList(children).contains(child)){
            return  false;
        }
        int index2 = 0 ;
        for(int j = 0;j < children.length;j++){
            if(children[j].equals(child)){

                child.setFamily(null);
                index2 = j;
            }

        }
        if(index2 < 0 || children == null || index2 > (children.length -1)){
            return false;
        }
        int length = children.length;
        Human [] newChildren = new Human[children.length - 1];

        for (int i = 0; i < index2; i++) {
            newChildren[i] = children[i];
        }
        for (int i = index2; i < children.length - 1; i++) {
            newChildren[i] = children[i + 1];
        }
        children = newChildren;
        if(length > children.length){
            return true;
        } else return false;

    }
    @Override
    public  String toString(){
        String str = "";
        for(int i = 0;i < children.length;i++){
            str = str + children[i].toString() ;
        }
        String message =  str + this.father.toString() + this.mother.toString() + this.pet.toString()  ;

        return message;
    }
    @Override
    public int hashCode(){
        int result = this.getMother() == null?0:this.getMother().hashCode();
        result = this.getFather() == null? result : result + this.getFather().hashCode();

        return result;
    }
    @Override
    public boolean equals(Object obj){

        if(obj == null){
            return  false;
        }
        if(!(obj.getClass() == Family.class)){
            return false;
        }
        Family family = (Family) obj;
        Human familyMother = family.getMother();
        Human familyFather = family.getFather();
        if((familyMother == this.mother || familyMother.equals(this.mother))  &&
                (familyFather == this.father || familyFather.equals(this.father))) {
            return true;
        }else  return false;

    }

    public static void main(String[] args) {
        Human Peter =new Human("Peter","Tomson",1960);
        Human Helen = new Human("Helen","Tomson",1965);
        Family Tomson = new Family(Peter,Helen);
        Pet Pusha = new DomesticCat("Pusha",5,(byte) 50);
        String [] habits =  new String [] {"eat","drink","sleep"};
        Pusha.setHabits(habits);
        Tomson.setPet(Pusha);
        Human Michael = new Human("Michael","Tomson",1994);
        Tomson.addChild(Michael);
        Tomson.toString();
        Tomson.deleteChild(0);
        System.out.println("-------------------");
        Tomson.toString();

    }

}
