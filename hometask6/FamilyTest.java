package hometask6;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyTest {
    private Family module;

    @BeforeEach
    public void setUp() {
        Human mother = new Human("mother","motherSurname",1978);
        Human father = new Human("father","fatherSurname",1976);
        Pet pet = new Dog("pet",3,(byte) 20);
        String [][] schedule = new String[2][2];
        module = new Family(mother,father);
        Human child = new Human("child","childSurname",1998);
        module.addChild(child);
        module.setPet(pet);

    }

    @Test
    public void testFamilyToString(){

        String actual = module.toString();
        String expected = "Human{name = child surname = childSurname year = 1998 iq = 0 schedule = null}"
                +"Human{name = father surname = fatherSurname year = 1976 iq = 0 schedule = null}" +
                "Human{name = mother surname = motherSurname year = 1978 iq = 0 schedule = null}"+
                "DOG{nickname=pet, age=3, trickLevel=20 , habits=null}";
        assertEquals(expected,actual);

    }
    @Test
    public void testCountFamily() {

        int actual = module.countFamily(module.getChildren());
        int expected = 3;
        assertEquals(expected,actual);

    }
    @Test
    public void testAddChild(){
        Human child2 = new Human("child2","surname",1999);
        module.addChild(child2);
        int actual = module.getChildren().length;
        int expected = 2;
        assertEquals(expected,actual);
    }
    @Test
    public void testDeleteChildWithWrongIndex(){

        int length = module.getChildren().length;
        int index = length + 1;
        module.deleteChild(index);
        int actual = module.getChildren().length;
        int expected = length;
        assertEquals(expected, actual);
    }
    @Test
    public void testDeleteChildIndex(){

        int length = module.getChildren().length;
        boolean bool = module.deleteChild(0);
        int actual = module.getChildren().length;
        int expected = length - 1;
        assertEquals(expected, actual);
        boolean actual2 = bool;
        boolean expected2 = true;
        assertEquals(expected2,actual2);
    }
    @Test
    public void testDeleteChildHuman(){
        Human child3 = new Human("sam","smith",32);
        module.addChild(child3);
        int length = module.getChildren().length;
        boolean bool = module.deleteChild(child3);
        int actual = module.getChildren().length;
        int expected = length -1 ;
        assertEquals(expected, actual);
        boolean actual2 = bool;
        boolean expected2 = true;
        assertEquals(expected2,actual2);
    }
    @Test
    public void testDeleteChildHumanWrongObject(){
        Human child3 = new Human();
        int length = module.getChildren().length;
        boolean bool = module.deleteChild(child3);
        int actual = module.getChildren().length;
        int expected = length ;
        assertEquals(expected, actual);
        boolean actual2 = bool;
        boolean expected2 = false;
        assertEquals(expected2,actual2);

    }
}


