package hometask6;

import java.util.Arrays;

 class  Fish extends Pet {



  public Fish(String nickName,int age,byte trickLevel){
   super(nickName,age,trickLevel);
   this.species = Species.FISH;

  }

  public  void respond (){
   System.out.println("Привет хозяин Я " + this.getNickName() +"cоскучилась");
  };

  @Override
  public   String  toString(){

   String message =  species + "{nickname=" + this.getNickName() +", age=" + this.getAge() +", trickLevel=" + this.getTrickLevel() +" , habits=" + Arrays.toString(this.getHabits()) + "}" ;
   System.out.println(message);
   return message;
  }


  public static void main(String[] args) {
   Fish fish = new Fish("goldfish",3,(byte) 10);
   fish.toString();
   System.out.println(fish.getSpecies());
  }

 }
