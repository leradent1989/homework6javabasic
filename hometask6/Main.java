package hometask6;

public class Main {

    public static void main(String[] args) {
        DomesticCat Mura = new DomesticCat("Mura",5,(byte) 70);
        Mura.toString();
        Mura.respond();
        Mura.foul();
        Dog  snoopy = new Dog("Mimi",5,(byte) 30);
        snoopy.setNickName("snoopy");
        snoopy.toString();
        snoopy.respond();
        RoboCat K5 = new RoboCat("Mimi",5,(byte) 70);
        K5.toString();
        K5.respond();
        K5.foul();
        Woman Mary = new Woman();
        System.out.println(Mary.doMakeUp("Done"));
        Mary.toString();
        Woman mother = new Woman();
        Man father = new Man();
        Family smith = new Family(mother,father);
        Mary.setFamily(smith);
        Pet Mimi = new Dog("Mimi",5,(byte) 60);
        Mimi.toString();
        smith.setPet(Mimi);
        Mary.greetPet(smith);
        Man George = new Man();
        System.out.println(George.fixCar("Fixed"));

    }
}
