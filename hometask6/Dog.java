package hometask6;

import java.util.Arrays;

public class Dog extends Pet implements PetInterface{




    public Dog(String nickName,int age,byte trickLevel) {
        super( nickName, age, trickLevel);
        species = Species.DOG;
    }

    public void respond() {
        System.out.println("Привет хозяин Я " + species+ " "+ this.getNickName() + " cоскучилcя");
    }

    public void foul(){
        System.out.println("Нужно замести следы ...");
    }


    public static void main(String[] args) {
        Dog  snoopy = new Dog("Mimi",5,(byte) 30);
        snoopy.setNickName("snoopy");
        snoopy.toString();
        snoopy.respond();
       System.out.println( snoopy.getSpecies());
    }

}
